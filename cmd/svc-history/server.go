package main

import (
	"context"
	"log"
	"os"

	"bitbucket.org/yuso-itauto-dev/svc-history/handlers"
	"bitbucket.org/yuso-itauto-dev/svc-history/models"

	elasticsearch "bitbucket.org/yuso-itauto-dev/svc-history/elastic"

	"time"

	"fmt"

	"github.com/namsral/flag"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/opentracer"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"

	elastic "gopkg.in/olivere/elastic.v5"

	"github.com/jasonlvhit/gocron"
	ot "github.com/opentracing/opentracing-go"
	otelastic "github.com/shidenkai0/opentracing-contrib/github.com/olivere/elastic"
)

const (
	service = "history-service"
)

var (
	hostIP = "172.17.0.1:8126"
)

func main() {
	log.Println("History service starting ...")

	dbURL := flag.String("database_url", "postgres://postgres:postgres@localhost:5432/svc-history", "database URL")
	dbPool := flag.Int("db_pool", 4, "database connection pool size")
	port := flag.String("port", "8000", "Port to listen on")
	appSecret := flag.String("app_secret", "secret", "App wide secret used for authentication purposes")
	backupOlderThan := flag.Int("backup_threshold_hours", 4380, "Backup traces older than this number of hours")
	tracing := flag.Bool("tracing", false, "Enable tracing")
	backupInterval := flag.Int("backup_interval_hours", 24, "Trace DB backup interval (h)")
	cleanup := flag.Bool("cleanup", false, "Cleanup Trace DB after backup (delete backed up traces)")
	esURL := flag.String("es_url", "http://localhost:9200", "ElasticSearch URL")
	esUser := flag.String("es_user", "elastic", "ElasticSearch User")
	esPassword := flag.String("es_password", "changeme", "ElasticSearch Password")
	// esURLTracesHistory := flag.String("es_url_traces_history", "http://localhost:9200", "ElasticSearch URL")
	// esUserTracesHistory := flag.String("es_user_traces_history", "elastic", "ElasticSearch User")
	// esPasswordTracesHistory := flag.String("es_password_traces_history", "changeme", "ElasticSearch Password")
	flag.Parse()

	if *tracing {
		t := opentracer.New(tracer.WithServiceName(service), tracer.WithAgentAddr(hostIP), tracer.WithGlobalTag("env", os.Getenv("RACK")))
		ot.SetGlobalTracer(t)
	}

	tc := otelastic.NewTracedHTTPClient()
	esClient, err := elastic.NewClient(elastic.SetURL(*esURL), elastic.SetBasicAuth(*esUser, *esPassword), elastic.SetSniff(false), elastic.SetErrorLog(log.New(os.Stdout, "[ElasticSearchError]", 0)), elastic.SetHttpClient(tc))
	if err != nil {
		log.Fatalln(err)
	}
	// esClientTracesHistory, err := elastic.NewClient(elastic.SetURL(*esURLTracesHistory), elastic.SetBasicAuth(*esUserTracesHistory, *esPasswordTracesHistory), elastic.SetSniff(false), elastic.SetErrorLog(log.New(os.Stdout, "[ElasticSearchError]", 0)), elastic.SetHttpClient(tc))
	// if err != nil {
	// 	log.Fatalln(err)
	// }

	//fmt.Printf("Setting up elasticsearch history index, name: %s, result: %v\n", elasticsearch.IndexName, setupIndex(esClientTracesHistory, elasticsearch.IndexName, elasticsearch.Mapping))
	fmt.Printf("Setting up elasticsearch history index, name: %s, result: %v\n", elasticsearch.PerMinuteIndexName, setupIndex(esClient, elasticsearch.PerMinuteIndexName, elasticsearch.Mapping))

	// Database Initialisation
	models.Init(*dbURL, *dbPool, esClient, esClient)
	defer models.Clean()
	thresholdDuration, _ := time.ParseDuration(fmt.Sprintf("%dh", *backupOlderThan))

	gocron.Every(uint64(*backupInterval)).Hours().Do(func() {
		err := models.Backup(models.BackupParams{
			OlderThan: time.Now().Add(-thresholdDuration),
			Cleanup:   *cleanup,
		})
		if err != nil {
			fmt.Printf("Backup: %v\n", err)
		}
		fmt.Println("Backup done")
	})
	gocron.Start()

	handlers.RunApi(*port, *appSecret)
}

func setupIndex(client *elastic.Client, indexName, mapping string) error {
	_, err := client.CreateIndex(indexName).BodyString(mapping).Do(context.Background())
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}
