package models

import (
	"database/sql"
	"log"

	"bitbucket.org/yuso-itauto-dev/svc-history"

	"bitbucket.org/yuso-itauto-dev/svc-history/postgres"

	es "bitbucket.org/yuso-itauto-dev/svc-history/elastic"

	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"github.com/shidenkai0/instrumentedsql"
	"github.com/shidenkai0/instrumentedsql/opentracing"

	"gopkg.in/olivere/elastic.v5"
)

var (
	db        *sqlx.DB
	collector history.TraceCollector
)

const jsonQuery = "SELECT row_to_json(rows) as data from (%s) rows;"

// Init initializes the database connection pool
func Init(dbURL string, poolSize int, c, tracesHistoryClient *elastic.Client) {
	driverName := "instrumented-pq"
	sql.Register(driverName, instrumentedsql.WrapDriver(&pq.Driver{}, instrumentedsql.WithTracer(opentracing.NewTracer())))
	var err error
	if db, err = sqlx.Connect(driverName, dbURL); err != nil {
		log.Fatalln("Database Connection Error", err)
	}
	db.SetMaxOpenConns(poolSize)
	collector = &history.MultiTraceCollector{
		Collectors: []history.TraceCollector{
			postgres.NewTraceCollector(db),
			es.NewTraceCollector(c, tracesHistoryClient),
		},
	}
}

// Clean closes the database connection pool, call it when you are done with database queries and no more need to be performed
// Typically used in your main program procedure with a defer clause
func Clean() {
	db.Close()
}
