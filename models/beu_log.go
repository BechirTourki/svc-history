package models

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"time"
)

type BeuActionLog struct {
	Id            int             `json:"id"`
	Action        string          `json:"action"`
	BeuId         int             `json:"back_end_user_id"`
	Model         string          `json:"model_type"`
	ModelId       int             `json:"model_id"`
	SaasCompanyID int             `json:"saas_company_id"`
	Data          json.RawMessage `json:"data"`
	CreatedAt     string          `json:"created_at"`
}

// SaveBackEndUserActionLog puts the given log in database
func SaveBackEndUserActionLog(ctx context.Context, bal BeuActionLog) error {
	queryStr := "insert into back_office_back_end_user_action_logs(action, back_end_user_id, model_type, model_id, saas_company_id, data) values($1, $2, $3, $4, $5, $6);"
	_, err := db.ExecContext(ctx, queryStr, bal.Action, bal.BeuId, bal.Model, bal.ModelId, bal.SaasCompanyID, bal.Data)
	if err != nil {
		log.Println("Error commiting driver state to DB", err)
		return err
	}
	return nil
}

// BeuActionLogByBeuId gets backend user logs by backend user id.
func BeuActionLogsByBeuId(ctx context.Context, scId, id int, from, to time.Time) ([]BeuActionLog, error) {
	queryStr := "SELECT row_to_json(rows) as data from (SELECT * from back_office_back_end_user_action_logs WHERE saas_company_id = $1 AND back_end_user_id = $2 AND created_at between $3 and $4 ORDER BY created_at) rows;"

	var (
		bals = make([]BeuActionLog, 0)
		res  []byte
	)
	rows, err := db.QueryContext(ctx, queryStr, scId, id, from, to)
	if err != nil {
		log.Println("Trouble Querying DB", err)
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var bal BeuActionLog
		if err = rows.Scan(&res); err != nil {
			log.Println("Trouble scanning results from DB query", err)
			return nil, err
		}
		if err := json.Unmarshal(res, &bal); err != nil {
			log.Println("Error Unmarshalling BackEndUser Log JSON", err)
			return nil, err
		}
		bals = append(bals, bal)
	}
	return bals, nil
}

// BeuActionLogsByModelTypeAndId gets backend user logs (self explanatory name)
func BeuActionLogsByModelTypeAndId(ctx context.Context, scId int, modelType string, modelId int, from, to time.Time) ([]BeuActionLog, error) {
	const baseQueryStr = "SELECT * from back_office_back_end_user_action_logs WHERE saas_company_id = $1 AND model_type = $2 AND created_at between $3 and $4"
	const optParamQueryStr = "SELECT * from back_office_back_end_user_action_logs WHERE saas_company_id = $1 AND model_type = $2 AND model_id = $3 AND created_at between $4 and $5"

	var (
		rows *sql.Rows
		err  error
	)

	if modelId < 0 {
		queryStr := fmt.Sprintf(jsonQuery, baseQueryStr)
		rows, err = db.QueryContext(ctx, queryStr, scId, modelType, from, to)
	} else {
		queryStr := fmt.Sprintf(jsonQuery, optParamQueryStr)
		rows, err = db.QueryContext(ctx, queryStr, scId, modelType, modelId, from, to)
	}

	if err != nil {
		log.Println("Trouble Querying DB", err)
		return nil, err
	}

	var bals = make([]BeuActionLog, 0)
	defer rows.Close()

	for rows.Next() {
		var (
			bal BeuActionLog
			res []byte
		)
		if err = rows.Scan(&res); err != nil {
			log.Println("Trouble scanning results from DB query", err)
			return nil, err
		}
		if err := json.Unmarshal(res, &bal); err != nil {
			log.Println("Error Unmarshalling BackEndUser Log JSON", err)
			return nil, err
		}
		bals = append(bals, bal)
	}
	return bals, nil
}
