package handlers

import (
	"fmt"

	othttp "github.com/shidenkai0/opentracing-contrib/net/http"

	"github.com/NYTimes/gziphandler"
	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"github.com/julienschmidt/httprouter"
	"github.com/justinas/alice"
	"github.com/urfave/negroni"
)

// RunApi starts the server on the given port
func RunApi(port, appSecret string) {

	// New JWT middleware
	jwtMiddleware := jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte(appSecret), nil
		},
		SigningMethod: jwt.SigningMethodHS256,
	})

	// Set Middleware to clear context at each new request
	commonHandlersTrustedServices := alice.New(jwtMiddleware.Handler, GetJwtSubCheckMiddleware("yuso-web,go-lp-notifs,svc-pricer"), corsHeaders, JsonHeaders)
	commonHandlersBeu := alice.New(jwtMiddleware.Handler, GetJwtSubCheckMiddleware("back_end_user,yuso-web"), corsHeaders, JsonHeaders)
	optHandlers := alice.New(corsHeaders)
	healthHandler := alice.New(JsonHeaders)

	// Instantiate a new HTTP router and set routes
	router := httprouter.New()
	router.Handler("GET", "/health", gziphandler.GzipHandler(healthHandler.ThenFunc(healthResponse)))
	router.Handler("GET", "/traces", gziphandler.GzipHandler(commonHandlersBeu.ThenFunc(GetTracesByTimeFrame)))
	router.Handler("OPTIONS", "/traces", optHandlers.ThenFunc(emptyResponse))
	router.Handler("POST", "/traces", commonHandlersTrustedServices.ThenFunc(AddTrace))
	router.Handler("GET", "/back_end_user_action_logs", gziphandler.GzipHandler(commonHandlersBeu.ThenFunc(GetBeuActionLogsHandler)))
	router.Handler("OPTIONS", "/back_end_user_action_logs", optHandlers.ThenFunc(emptyResponse))
	router.Handler("POST", "/back_end_user_action_logs", commonHandlersTrustedServices.ThenFunc(PostBeuActionLogsHandler))
	router.Handler("GET", "/model_names", gziphandler.GzipHandler(commonHandlersTrustedServices.ThenFunc(GetModelNames)))
	router.Handler("POST", "/push_notification_logs", commonHandlersTrustedServices.ThenFunc(postPushNotifLogsHandler))
	router.Handler("GET", "/push_notification_logs", gziphandler.GzipHandler(commonHandlersBeu.ThenFunc(getPushNotifLogsHandler)))
	router.Handler("OPTIONS", "/push_notification_logs", optHandlers.ThenFunc(emptyResponse))

	n := negroni.New()
	n.Use(negroni.NewRecovery())
	n.Use(negroni.NewLogger())
	n.UseHandler(othttp.ServerMiddleware(router))

	// Start listening for HTTP requests
	n.Run(fmt.Sprintf(":%s", port))
}
