package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/yuso-itauto-dev/svc-geo"
	"bitbucket.org/yuso-itauto-dev/svc-history"
	"bitbucket.org/yuso-itauto-dev/svc-history/models"

	"github.com/asaskevich/govalidator"
)

// jsonTime is an easily ISO-formattable extension of time.Time
type jsonTime struct {
	time.Time
	f string
}

// setISOFormat sets the format string of a jsonTime
func (j *jsonTime) setISOFormat() {
	j.f = "2006-01-02T15:04:05.999Z"
}

// format is a utility function used by marshalling methods to get the jsonTime as a formatted string
func (j jsonTime) format() string {
	return j.Time.Format(j.f)
}

// MarshalText overrides time.Time's text marshalling method using jsonTime's embedded format string
func (j jsonTime) MarshalText() ([]byte, error) {
	return []byte(j.format()), nil
}

// MarshalJSON overrides time.Time's JSON marshalling methods using jsonTime's embedded format string
func (j jsonTime) MarshalJSON() ([]byte, error) {
	return []byte(`"` + j.format() + `"`), nil
}

// Trace represents a driver position in terms of geolocation as a cartesian point, driver state, and time information
type Trace struct {
	DriverID  int      `json:"driver_id"`
	L         Point    `json:"location"`
	State     int      `json:"state"`
	SentAt    jsonTime `json:"sent_at"`
	CreatedAt jsonTime `json:"created_at"`
	Speed     float32  `json:"speed"`
	Accuracy  float32  `json:"accuracy"`
}

// Point defines a point as cartesian coordinates
type Point struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

// GetTracesByTimeFrame is an http handler for getting driver traces for a given driver and time frame
func GetTracesByTimeFrame(w http.ResponseWriter, r *http.Request) {
	encoder := json.NewEncoder(w)

	from, err := time.Parse("2006-01-02T15:04", r.URL.Query().Get("from"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		encoder.Encode(ApiResponse{false, err.Error()})
		return
	}

	to, err := time.Parse("2006-01-02T15:04", r.URL.Query().Get("to"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		encoder.Encode(ApiResponse{false, err.Error()})
		return
	}

	var dts []geo.DriverTrace
	driverID, err := strconv.Atoi(r.URL.Query().Get("driver_id"))

	if err != nil { // No driver id supplied
		if to.Sub(from) > time.Minute {
			to = from.Add(time.Minute)
		}
		dts, err = models.AllTracesByTimeFrame(r.Context(), from, to)
	} else {
		if to.Sub(from) > 24*60*time.Minute {
			to = from.Add(24 * 60 * time.Minute)
		}
		dts, err = models.TracesByTimeFrame(r.Context(), driverID, from, to)
	}

	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		encoder.Encode(ApiResponse{false, err.Error()})
	}

	ts := make([]Trace, 0)

	for _, dt := range dts {
		t := Trace{
			DriverID: dt.ID,
			L: Point{
				X: dt.Location.Lng,
				Y: dt.Location.Lat,
			},
			State:     dt.Status,
			SentAt:    jsonTime{Time: dt.UpdatedAt},
			CreatedAt: jsonTime{Time: dt.UpdatedAt},
			Speed:     dt.Speed,
			Accuracy:  dt.Accuracy,
		}

		t.CreatedAt.setISOFormat()
		t.SentAt.setISOFormat()
		if math.IsInf(float64(t.Speed), 0) {
			t.Speed = 0
		}
		ts = append(ts, t)
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		encoder.Encode(ApiResponse{false, err.Error()})
		return
	}
	fmt.Println("Driver Id, From, To :", driverID, from, to)
	err = encoder.Encode(ApiResponse{true, ts})
	if err != nil {
		encoder.Encode(ApiResponse{false, err.Error()})
	}
}

func AddTrace(w http.ResponseWriter, r *http.Request) {
	var dt history.DriverTrace

	if err := json.NewDecoder(r.Body).Decode(&dt); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Printf("handlers.AddTrace: %v\n", err)
		return
	}
	if valid, err := govalidator.ValidateStruct(dt); !valid || err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err := models.AddTrace(r.Context(), dt)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Printf("handlers.AddTrace: %v\n", err)
		return
	}
	w.WriteHeader(http.StatusOK)
}
