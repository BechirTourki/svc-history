CREATE TABLE public.back_office_back_end_user_action_logs (
    id integer NOT NULL,
    action text,
    back_end_user_id integer,
    model_type text,
    model_id bigint,
    saas_company_id integer,
    data json,
    created_at timestamp without time zone DEFAULT now()
);

CREATE SEQUENCE public.back_office_back_end_user_action_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE TABLE public.driver_traces (
    id bigint NOT NULL,
    driver_id integer,
    location point,
    state integer,
    sent_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    speed real DEFAULT 0,
    accuracy real DEFAULT 0
);

CREATE SEQUENCE public.driver_traces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.push_notification_logs (
    id integer NOT NULL,
    back_end_user_id integer,
    saas_company_id integer,
    customer_id integer,
    drivers_ids integer[],
    data json,
    created_at timestamp without time zone DEFAULT now()
);

CREATE SEQUENCE public.push_notification_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE INDEX bo_beu_action_logs_back_end_user_idx ON public.back_office_back_end_user_action_logs USING btree (back_end_user_id);
CREATE INDEX bo_beu_action_logs_model_idx ON public.back_office_back_end_user_action_logs USING btree (model_id);
CREATE INDEX bo_beu_action_logs_model_type_idx ON public.back_office_back_end_user_action_logs USING btree (model_type);
CREATE INDEX bo_beu_action_logs_multiple_idx ON public.back_office_back_end_user_action_logs USING btree (created_at, saas_company_id, back_end_user_id);
CREATE INDEX bo_beu_action_logs_multiple_idx_second ON public.back_office_back_end_user_action_logs USING btree (model_type, model_id, back_end_user_id);
CREATE INDEX bo_beu_action_logs_saas_company_idx ON public.back_office_back_end_user_action_logs USING btree (saas_company_id);
CREATE INDEX cat ON public.driver_traces USING btree (created_at);
CREATE INDEX index_driver_traces_on_driver_id ON public.driver_traces USING btree (driver_id);
CREATE INDEX index_driver_traces_on_driver_id_and_created_at ON public.driver_traces USING btree (driver_id, created_at);
CREATE INDEX push_notifications_logs_beu_idx ON public.push_notification_logs USING btree (back_end_user_id);
CREATE INDEX push_notifications_logs_created_at_idx ON public.push_notification_logs USING btree (created_at);
CREATE INDEX push_notifications_logs_multiple_idx ON public.push_notification_logs USING btree (created_at, saas_company_id, back_end_user_id);
CREATE INDEX push_notifications_logs_saas_company_idx ON public.push_notification_logs USING btree (saas_company_id);

