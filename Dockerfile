FROM golang:1.15 AS build
ARG SSH_PRIVATE_REPOSITORY_KEY
RUN mkdir -p ~/.ssh && umask 0077 && echo $SSH_PRIVATE_REPOSITORY_KEY | base64 --decode > ~/.ssh/id_rsa \
    && git config --global url."git@bitbucket.org:".insteadOf https://bitbucket.org/ \
    && ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
RUN cat ~/.ssh/id_rsa
ADD . /app
WORKDIR /app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app

FROM alpine
RUN apk --no-cache add ca-certificates
RUN mkdir /app
WORKDIR /app
COPY --from=build /app/app .
ENTRYPOINT [ "./app" ]