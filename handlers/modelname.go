package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/yuso-itauto-dev/svc-history/models"
)

func GetModelNames(w http.ResponseWriter, r *http.Request) {
	scId, err := strconv.Atoi(r.URL.Query().Get("saas_company_id"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var mns []models.ModelName
	mns, err = models.GetModelNames(r.Context(), scId)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	fmt.Println("ModelNames For Saas Company", scId)
	json.NewEncoder(w).Encode(mns)
}
