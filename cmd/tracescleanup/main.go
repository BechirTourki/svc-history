package main

import (
	"flag"
	"fmt"
	"time"

	"bitbucket.org/yuso-itauto-dev/svc-history/models"

	_ "github.com/lib/pq"
)

func main() {
	dbUrl := flag.String("dburl", "", "Traces database URL")
	cleanup := flag.Bool("c", false, "Cleanup the database")
	maxAge := flag.Int("max-age", 4380, "Max age of driver traces (hours)")
	flag.Parse()
	maxAgeDuration, _ := time.ParseDuration(fmt.Sprintf("%dh", *maxAge))
	models.Init(*dbUrl, 1)
	params := models.BackupParams{
		Cleanup:   *cleanup,
		OlderThan: time.Now().Add(-maxAgeDuration),
	}
	fmt.Println(models.Backup(params))
}
// DEPRECATED
// DEPRECATED // DEPRECATED // DEPRECATED // DEPRECATED // DEPRECATED // DEPRECATED // DEPRECATED // DEPRECATED
// DEPRECATED // DEPRECATED // DEPRECATED // DEPRECATED // DEPRECATED // DEPRECATED // DEPRECATED // DEPRECATED