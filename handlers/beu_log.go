package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/yuso-itauto-dev/svc-history/models"
)

func PostBeuActionLogsHandler(w http.ResponseWriter, r *http.Request) {
	var (
		decoder = json.NewDecoder(r.Body)
		encoder = json.NewEncoder(w)
		resp    = ApiResponse{false, nil}
		bal     models.BeuActionLog
	)

	err := decoder.Decode(&bal)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		encoder.Encode(resp)
	}

	err = models.SaveBackEndUserActionLog(r.Context(), bal)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		encoder.Encode(resp)
	}

	resp = ApiResponse{true, nil}
	encoder.Encode(resp)
}

func GetBeuActionLogsHandler(w http.ResponseWriter, r *http.Request) {
	var (
		encoder   = json.NewEncoder(w)
		resp      = ApiResponse{false, nil}
		scId, err = strconv.Atoi(r.URL.Query().Get("saas_company_id"))
	)

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		encoder.Encode(resp)
		return
	}

	from, err := time.Parse("2006-01-02T15:04", r.URL.Query().Get("from"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		encoder.Encode(resp)
		return
	}

	to, err := time.Parse("2006-01-02T15:04", r.URL.Query().Get("to"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		encoder.Encode(resp)
		return
	}

	var (
		beuIdStr     = r.URL.Query().Get("back_end_user_id")
		modelTypeStr = r.URL.Query().Get("model_type")
		modelIdstr   = r.URL.Query().Get("model_id")
	)

	var bals []models.BeuActionLog

	if beuIdStr != "" && modelTypeStr == "" {
		beuId, _ := strconv.Atoi(r.URL.Query().Get("back_end_user_id"))
		bals, err = models.BeuActionLogsByBeuId(r.Context(), scId, beuId, from, to)
	} else if beuIdStr == "" && modelTypeStr != "" && modelIdstr == "" {
		bals, err = models.BeuActionLogsByModelTypeAndId(r.Context(), scId, modelTypeStr, -1, from, to)
	} else if beuIdStr == "" && modelTypeStr != "" && modelIdstr != "" {
		modelId, err := strconv.Atoi(modelIdstr)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			encoder.Encode(resp)
			return
		}
		bals, err = models.BeuActionLogsByModelTypeAndId(r.Context(), scId, modelTypeStr, modelId, from, to)
	}

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		encoder.Encode(resp)
		return
	}
	resp = ApiResponse{true, bals}
	encoder.Encode(resp)
}

/*func periodicNotifications(period int) {
	ticker := time.Tick(time.Duration(period) * time.Second)
	notif := models.Notification{SaasOfficeId: 1, Type: "driver_notif", Content: fmt.Sprint(time.Now())}

    for {
		select {
		case <- ticker:
			select {
			case clientchan := <-lpchan:
				notif.Content = time.Now().Format("2006-01-02T15:04:05.999Z07:00")
				notif.SaasOfficeId++
				clientchan.NotifChan <- notif
				log.Println("Sending notification with saas_office_id", notif.SaasOfficeId)
			default:
			}
		}

    }
}*/
