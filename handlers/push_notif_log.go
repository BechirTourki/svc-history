package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	"strconv"
	"time"

	"bitbucket.org/yuso-itauto-dev/svc-history/models"
)

// postPushNotifLogsHandler handles incoming HTTP requests for posting push notifications logs
func postPushNotifLogsHandler(w http.ResponseWriter, r *http.Request) {
	var (
		decoder = json.NewDecoder(r.Body)
		encoder = json.NewEncoder(w)
		resp    = ApiResponse{false, nil}
		pnl     models.PushNotifLog
	)

	err := decoder.Decode(&pnl)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		encoder.Encode(resp)
		return
	}

	err = models.SavePushNotifLog(r.Context(), pnl)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		encoder.Encode(resp)
		return
	}

	resp = ApiResponse{true, nil}
	encoder.Encode(resp)
}

// getPushNotifLogsHandler handles incoming HTTP requests for getting push notifications logs
func getPushNotifLogsHandler(w http.ResponseWriter, r *http.Request) {
	var (
		encoder   = json.NewEncoder(w)
		resp      = ApiResponse{false, nil}
		scID, err = strconv.Atoi(r.URL.Query().Get("saas_company_id"))
	)

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		encoder.Encode(resp)
		return
	}

	from, err := time.Parse("2006-01-02T15:04", r.URL.Query().Get("from"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		encoder.Encode(resp)
		return
	}

	to, err := time.Parse("2006-01-02T15:04", r.URL.Query().Get("to"))
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		encoder.Encode(resp)
		return
	}

	var pnls []models.PushNotifLog
	pnls, err = models.GetPushNotifLogs(r.Context(), scID, from, to)

	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		encoder.Encode(resp)
		return
	}
	resp = ApiResponse{true, pnls}
	encoder.Encode(resp)
}
