package models

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"
)

type PushNotifLog struct {
	Id         int             `json:"id"`
	BeuId      int             `json:"back_end_user_id"`
	ScId       int             `json:"saas_company_id"`
	CustomerId int             `json:"customer_id"`
	DriverIds  []int           `json:"drivers_ids"`
	Data       json.RawMessage `json:"data"`
	CreatedAt  string          `json:"created_at"`
}

type intArray []int

// Value formats an intArray to the standard postgres syntax
func (ia intArray) Format() string {
	res := "{"

	for _, v := range ia {
		res = res + fmt.Sprintf("%d,", v)
	}

	if strings.HasSuffix(res, ",") {
		res = res[:len(res)-1]
	}

	res = res + "}"
	fmt.Println("IntArray format", res)
	return res
}

// GetPushNotifLogs retrieves push notification logs between two datetimes from the database.
func GetPushNotifLogs(ctx context.Context, scId int, from, to time.Time) ([]PushNotifLog, error) {
	const sqlStr = "SELECT * FROM push_notification_logs WHERE (saas_company_id = $1 AND created_at BETWEEN $2 AND $3)"
	queryStr := fmt.Sprintf(jsonQuery, sqlStr)
	rows, err := db.QueryContext(ctx, queryStr, scId, from, to)
	if err != nil {
		log.Println("Trouble Querying DB", err)
		return nil, err
	}

	var pnls []PushNotifLog
	defer rows.Close()

	for rows.Next() {
		var (
			pnl PushNotifLog
			res []byte
		)

		if err = rows.Scan(&res); err != nil {
			log.Println("Trouble scanning results from DB query", err)
			return nil, err
		}

		if err := json.Unmarshal(res, &pnl); err != nil {
			log.Println("Error Unmarshalling BackEndUser Log JSON", err)
			return nil, err
		}
		pnls = append(pnls, pnl)
	}
	return pnls, nil
}

// SavePushNotifLog saves a push notification logs to the database.
func SavePushNotifLog(ctx context.Context, pnl PushNotifLog) error {
	queryStr := "insert into push_notification_logs(back_end_user_id, saas_company_id, customer_id, drivers_ids, data) values($1, $2, $3, $4, $5);"
	fmt.Println("intArray", intArray(pnl.DriverIds))
	fmt.Println("Data", pnl.Data)
	_, err := db.ExecContext(ctx, queryStr, pnl.BeuId, pnl.ScId, pnl.CustomerId, intArray(pnl.DriverIds).Format(), pnl.Data)
	if err != nil {
		log.Println("Error commiting push notification log to DB", err)
		return err
	}
	return nil
}
