package history

import (
	"context"
	"time"
)

// DriverTrace stores information about a driver (mainly spatiotemporal), should be identical to geo.DriverTrace
type DriverTrace struct {
	ID                  int       `json:"id"` // driver id
	Location            Point     `json:"location"`
	Status              int       `json:"status"`
	Speed               float32   `json:"speed"`
	Accuracy            float32   `json:"accuracy"`
	SentAt              time.Time `json:"sent_at"` //Timestamp would be a better name
	OfficeID            int       `json:"saas_office_id"`
	InUseCarID          int       `json:"driver_car_in_use_id"`
	CurrentRideID       int       `json:"current_ride_id"`
	PrimaryVehicleIDs   []int     `json:"primary_vehicle_ids"`
	SecondaryVehicleIDs []int     `json:"secondary_vehicle_ids"`
}

// Point represents geo coordinates in the standard decimal degree format
type Point struct {
	Lat float64 `json:"lat" db:"lat"`
	Lng float64 `json:"lng" db:"lng"`
}

//
type TraceCollector interface {
	Add(ctx context.Context, dt DriverTrace) error
}

// TraceSeeker allows to search for driver traces by timeframe
type TraceSeeker interface {
	TracesByTimeFrame(ctx context.Context, from time.Time, to time.Time, driverId int) ([]DriverTrace, error)
	AllTracesByTimeFrame(ctx context.Context, from time.Time, to time.Time) ([]DriverTrace, error)
}

// MultiTraceCollector allows adding driver traces to multiple collectors
type MultiTraceCollector struct {
	Collectors []TraceCollector
}

func (m *MultiTraceCollector) Add(ctx context.Context, dt DriverTrace) error {
	var Err error
	for _, c := range m.Collectors {
		if err := c.Add(ctx, dt); err != nil {
			Err = err
		}
	}
	return Err
}
