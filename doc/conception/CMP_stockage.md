Composant : Stockage des traces
===============================

Définition fonctionnelle
------------------------

### Rôle
Stockage des traces

### Événement déclencheur 
Appel web service

### Comportement en cas d’erreur

Erreur HTTP

### Actions réalisées

- Pour les traces chauffeur : 
    * Aucun traitement sur les données n'est réalisé.
    * Stockage dans PostgreSQL d'une sous-partie des informations de chaque trace.
    * Filtrage des données pour ne sélectionner qu'une trace chauffeur par minute pour un chauffeur 
      donné, puis stockage ElasticSearch. Toutes les informations de la trace sont stockées. Les 
      données stockées dans ElasticSearch ne sont pas utilisées par le composant de requêtage, mais 
      par un autre service, pour des requêtes complexes ou de l'import/export.
- Pour les traces des données métier gérées dans yuso-web :
    - Aucun traitement sur les données n'est réalisé.
    - Stockage dans PostgreSQL uniquement
- Pour les traces des notifications envoyées aux utilisateurs : 
    - Aucun traitement sur les données n'est réalisé.
    - Stockage dans PostgreSQL uniquement

### Interfaces – Services web exposés

#### Stockage des traces chauffeur

URL
: /traces

Méthode HTTP
: POST

#### Stockage des traces des données métier gérées dans yuso-web

URL
: /back_end_user_action_logs

Méthode HTTP
: POST

#### Stockage des traces des notifications envoyées aux utilisateurs

URL
: /push_notification_logs

Méthode HTTP
: POST
