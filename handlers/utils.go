package handlers

import (
	"net/http"

	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
)

// Wrapper to make httprouter.Handle compatible with standard http.Handler interface, enables to use any standard
// httprouter middleware
func WrapHandler(h http.Handler) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		context.Set(r, "params", ps)
		h.ServeHTTP(w, r)
	}
}

type ApiResponse struct {
	Success bool        `json:"success"`
	Results interface{} `json:"results"`
}

func emptyResponse(w http.ResponseWriter, r *http.Request) {
}

func healthResponse(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`{"state": "up"}`))
}