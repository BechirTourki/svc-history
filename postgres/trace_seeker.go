package postgres

import (
	"context"
	"time"

	"bitbucket.org/yuso-itauto-dev/svc-history"

	"github.com/jmoiron/sqlx"
)

type TraceSeeker struct {
	db *sqlx.DB
}

func (ts *TraceSeeker) TracesByTimeFrame(ctx context.Context, from time.Time, to time.Time, driverId int) ([]history.DriverTrace, error) {
	return nil, nil
}

func (ts *TraceSeeker) AllTracesByTimeFrame(ctx context.Context, from time.Time, to time.Time) ([]history.DriverTrace, error) {
	return nil, nil
}
