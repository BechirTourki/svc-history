Composant : Requêtage sur les traces
====================================

Définition fonctionnelle
------------------------

### Rôle
Requêtage sur les traces

### Événement déclencheur 
Appel web service

### Comportement en cas d’erreur
Erreur HTTP

### Actions réalisées

- Pour les traces chauffeur : 
    * Critères pour requêtes : période de recherche (heure début, heure fin, max : 1 journée),
      id chauffeur
    * Pas de pagination
    * Pas de tri, ordre chronologique
- Pour les traces des données métier gérées dans yuso-web. 2 types de requêtes : 
    * Par opérateur : pour voir tous les changements réalisés, dans un intervalle de temps 
      donné. Pas de limitation sur le volume de données renvoyées.
    * Par type de modèle de données, avec un ID du modèle, et un intervalle de temps. Pas de 
      limitation sur le volume de données renvoyées.
- Pour les traces des notifications envoyées aux utilisateurs : 
    * Critères : par intervalle de temps uniquement. Pas de limitation sur le volume de données
      renvoyées.

### Interfaces – Services web exposés

#### Requêtage des traces chauffeur

URL
: /traces

Méthode HTTP
: GET

#### Requêtage des traces des données métier gérées dans yuso-web

URL
: /back_end_user_action_logs

Méthode HTTP
: GET

#### Récupération des noms des données métier gérées

URL
: /model_names

Méthode HTTP
: GET

#### Requêtage des traces des notifications envoyées aux utilisateurs

URL
: /push_notification_logs

Méthode HTTP
: GET
