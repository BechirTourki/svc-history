## Purpose

This service stores and queries various history data (back-office users actions, driver traces). It also moves driver traces to cold storage (AWS S3) in order to keep old driver traces in back-up.
(driver traces are the driver latitude and longitude with a timestamp and a status)

##Configuration

Environment variables list:

|          Key           | Default |  Unit  | Mandatory | USED ? |                          Comment                          |
| :--------------------: | :-----: | :----: | :-------: | :----: | :-------------------------------------------------------: |
|       APP_SECRET       |         | string |    yes    |  yes   |         Shared Yuso secret to encode/decode JWT.          |
|   AWS_ACCESS_KEY_ID    |         | string |    yes    |  yes   |                  AWS S3 bucket access id                  |
|       AWS_REGION       |         | string |    yes    |  yes   |                   AWS S3 bucket region                    |
|     AWS_S3_BUCKET      |         | string |    yes    |  yes   |                       AWS S3 bucket                       |
| AWS_SECRET_ACCESS_KEY  |         | string |   true    |  yes   |                 AWS S3 bucket access key                  |
| BACKUP_INTERVAL_HOURS  |   24    |  int   |    no     |  yes   |         Frequency of the transfer to cold storage         |
| BACKUP_THRESHOLD_HOURS |  4380   |  int   |    no     |  yes   |      Age of the records to transfer to cold storage       |
|        CLEANUP         |  false  |  bool  |    no     |  yes   | Clean DB once record have been transfered to cold storage |
|        DB_POOL         |    4    |  int   |    no     |  yes   |                       DB pool size                        |
|        TRACING         |  false  |  bool  |    no     |  yes   |                      Enables the APM                      |
|      DATABASE_URL      |         | string |    yes    |  yes   |      A connection string to the postgreSQL database       |
|         ES_URL         |         | string |    yes    |  yes   |       URL of the “per minute” traces ElasticSearch        |
|        ES_USER         |         | string |    yes    |  yes   |     Username of the “per minute” traces ElasticSearch     |
|      ES_PASSWORD       |         | string |    yes    |  yes   |     Username of the “per minute” traces ElasticSearch     |

Databases:

|                             ENV Variable                              |       type        |                                                        usage                                                         |
| :-------------------------------------------------------------------: | :---------------: | :------------------------------------------------------------------------------------------------------------------: |
|                          PostgreSQL database                          | Long term storage |                         Used for record that are not driver traces related (e.g. model logs)                         |
|               ElasticSearch (per minute driver traces)                |                   | In regard to this service, contains the driver traces indexes every minute (only one record per driver, per minute). |
| Note that it is mutualized (same instance) with svc-driver-statistics |

##Processes

The list of processes for this micro service, e.g. a web process (listening to HTTP call), a worker process (consuming message), a scheduler process (triggering job based on a schedule), etc. In short, the different containers we will launch to make the micro service work correctly.

|                 Process Name                  | Command |                        Comment                         |  Readiness  |
| :-------------------------------------------: | :-----: | :----------------------------------------------------: | :---------: |
|                      web                      |  . app  |         Compiled executable of cmd/svc-history         | GET /health |
| tracescleanup (to run manually, periodically) |  . app  | LEGACY Compiled executable of cmd/tracescleanup LEGACY |    None     |

##How to work on this micro service

###Installation

You need:
- 2 running ElasticSearch cluster (can be running locally, see https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html)
- a PostgreSQL database
- Golang

###Tests

No test on this service.

###Compile

`docker build . -t svc-history --build-arg SSH_PRIVATE_REPOSITORY_KEY=$SSH_PRIVATE_REPOSITORY_KEY`

###Run in local

**Legacy but can be used to test in local.**

`docker-compose up --build`

Or compile the Go binary.

```shell
cd cmd/svc-history
go build server.go
./server
```

###Testing CircleCI configuration

Not available
