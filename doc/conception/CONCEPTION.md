Description fonctionnelle
=========================

Ce service permet de conserver des historiques de données, et d'effectuer des recherches sur ces 
historiques.  

Différents types d'historiques sont disponibles : 

- Traces chauffeur, envoyées par svc-notifications
- Traces des données métier gérées dans yuso-web :
    * Traces des actions sur les règles de prix (à ne pas confondre avec les pricing rule logs)
    * Agenda chauffeurs : trace de chaque modification d'un événement dans l'agenda, avec l'ancienne
      valeur et la nouvelle valeur.
    * 21 autres, déclarés dans un fichier de configuration dans le code du service
- Traces des notifications envoyées aux utilisateurs : 
    * notifications envoyées par svc-notifications
    * notifications envoyées par yuso-web aux clients (yuso-web envoie directement ces notifications 
      à PushWoosh, sans passer par svc-notifications). 
      NB : Quand l'application mobile du client s'ouvre, elle s'enregistre auprès de yuso-web, en
      fournissant son identifiant Pushwoosh, afin de recevoir les notifications natives de 
      Pushwoosh. yuso-web récupère dans svc-mobile-apps les secrets liés au gestionnaire de flotte 
      pour envoyer des notifications via PushWoosh.
  
Architecture logicielle
=======================

Description
-----------

Le service comporte deux composants : 

- [Stockage des traces](CMP_stockage.md)
- [Requêtage sur les traces](CMP_requetage.md)

Modèle de données
-----------------

### Traces chauffeur

- Heure de la trace
- Géolocalisation du chauffeur horodatée
- Précision GPS du smartphone
- Statut du chauffeur : non disponible, en pause, disponible
- Identifiant de la course en course 
- Prestations primaires disponibles pour le véhicule en cours d'utilisation par le chauffeur 
- Prestations secondaires disponibles pour le véhicule en cours d'utilisation par le chauffeur
- Identifiant de la sous-division du gestionnaire de flotte à laquelle le chauffeur est rattaché
- Identifiant du véhicule en cours d'utilisation
 
### Traces des données métier gérées dans yuso-web

- Type de modèle
- ID backend user
- Identifiant du gestionnaire de flotte
- Données : correspond à une liste des changements de valeurs pour chaque attribut. Selon le cas :
    * L'ancienne valeur et la nouvelle valeur sont fournies
    * Seule la nouvelle valeur est fournie

### Traces des notifications envoyées aux utilisateurs

- Titre
- Type d'icône de la notification
- Contenu du message
- Type de son
- Destinataires cibles (push tokens)

Composants transverses
----------------------

### Authentification

- Routes en POST, appelées uniquement par d'autres services, via un JWT, généré par les services 
  eux-mêmes, en mettant leur nom dans l'attribut `sub`. 
- Routes en GET, appelées par yuso-web (JWT généré par lui-même) ou l'application web backoffice 
  (pour les opérateurs), JWT généré par yuso-web.

Choix d'implémentation
----------------------

### Technologies utilisées

- GoLang

### Politique de traitement des exceptions
Rollbar n'est pas utilisé.  
Les erreurs sont tracées.  

### Politique des traces

- Erreurs tracées
- Requêtes et réponses HTTP tracées
- Logs métier

### Considérations de test
Pas de tests

### Considérations de sécurité

- Chiffrements des données mis en place : aucun
- Failles de sécurité connues :
    * Pas de limitations sur les traces récupérées par requêtes sur les traces de type yuso-web et 
      push notifications. Peut impliquer une rupture de service, et être un point d'attaque.
    * La BDD ElasticSearch n'est jamais purgée, le volume peut être important.

### Guides/astuces pour les évolutions futures
Aucun

### Guides/astuces pour la maintenance
Aucun

Modèle de données physique
--------------------------

- PostgreSQL : 3 tables, 1 par type de traces
- ElasticSearch v6.8 : partagé avec svc-driver-statistics, qui effectue des requêtes

### Scripts de BDD
Voir code

Maintenance et exploitation
---------------------------

Backup configurable (via variables d'environnement) :

- Au bout de 3 mois (configurable), les données sont envoyées dans un S3 et supprimées de la BDD 
  PostgreSQL.
- Les traces stockées dans ElasticSearch ne sont jamais supprimées. Il faut donc surveiller la 
  taille des données ElasticSearch.

Procédures manuelles
--------------------

- Si le volume de données dans ElasticSearch devient trop important, il faudra faire évoluer la 
  taille du stockage.

Points de vigilance
-------------------

Voir `Maintenance et exploitation` et `Considérations de sécurité`.
