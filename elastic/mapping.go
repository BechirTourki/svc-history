package elastic

import (
	"time"

	"bitbucket.org/yuso-itauto-dev/svc-history"
)

// ElasticSearch index and mapping to be used for History Service
const (
	IndexName          = "driver_history"
	PerMinuteIndexName = "driver_history_per_minute_new"
	Mapping            = `{
									"settings":{
										"number_of_shards":5,
										"number_of_replicas":1
									},
									"mappings":{
										"trace":{
											"properties":{
												"driver_id":{
													"type":"integer"
												},
												"saas_office_id":{
													"type":"integer"
												},
												"driver_car_in_use_id":{
													"type":"integer"
												},
												"primary_vehicle_ids":{
													"type":"integer"
												},
												"secondary_vehicle_ids":{
													"type":"integer"
												},
												"vehicle_ids":{
													"type":"integer"
												},
												"current_ride_id":{
													"type":"integer"
												},
												"location":{
													"type":"geo_shape",
													"points_only":true
												},
												"status":{
													"type":"integer"
												},
												"speed":{
													"type":"float"
												},
												"accuracy":{
													"type":"float"
												},
												"sent_at":{
													"type":"date"
												}
											}
										}
									}
								}`
	Type = "trace"
)

// ElasticDriverTrace stores information about a driver (mainly spatiotemporal)
type ElasticDriverTrace struct {
	ID                  int                  `json:"driver_id"` // driver id
	OfficeID            int                  `json:"saas_office_id"`
	Location            ElasticGeoShapePoint `json:"location"`
	Status              int                  `json:"status"`
	Speed               float32              `json:"speed"`
	Accuracy            float32              `json:"accuracy"`
	SentAt              time.Time            `json:"sent_at"`
	InUseCarID          int                  `json:"driver_car_in_use_id"`
	CurrentRideID       int                  `json:"current_ride_id"`
	PrimaryVehicleIDs   []int                `json:"primary_vehicle_ids"`
	SecondaryVehicleIDs []int                `json:"secondary_vehicle_ids"`
	VehicleIDs          []int                `json:"vehicle_ids"`
}

// TODO: ElasticPoint is not used anymore
// ElasticPoint represents geo coordinates in the standard decimal degree format in a way friendlier to ElasticSearch's geo_point convention
type ElasticPoint struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lon"`
}

type ElasticGeoShapePoint struct {
	Type        string    `json:"type"`
	Coordinates []float64 `json:"coordinates"`
}

func toElasticDriverTrace(dt history.DriverTrace) ElasticDriverTrace {
	return ElasticDriverTrace{
		ID:       dt.ID,
		OfficeID: dt.OfficeID,
		Location: ElasticGeoShapePoint{
			Type:        "Point",
			Coordinates: []float64{dt.Location.Lng, dt.Location.Lat},
		},
		Status:              dt.Status,
		Speed:               dt.Speed,
		Accuracy:            dt.Accuracy,
		SentAt:              dt.SentAt,
		InUseCarID:          dt.InUseCarID,
		CurrentRideID:       dt.CurrentRideID,
		PrimaryVehicleIDs:   dt.PrimaryVehicleIDs,
		SecondaryVehicleIDs: dt.SecondaryVehicleIDs,
		VehicleIDs:          append(dt.PrimaryVehicleIDs, dt.SecondaryVehicleIDs...),
	}
}
