module bitbucket.org/yuso-itauto-dev/svc-history

go 1.15

require (
	bitbucket.org/yuso-itauto-dev/svc-geo v0.0.0-20201102150019-d73f367b83f5
	github.com/NYTimes/gziphandler v1.1.1
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef
	github.com/auth0/go-jwt-middleware v0.0.0-20201030150249-d783b5c46b39
	github.com/aws/aws-sdk-go v1.35.24
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/context v1.1.1
	github.com/jasonlvhit/gocron v0.0.1 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/justinas/alice v1.2.0
	github.com/lib/pq v1.8.0
	github.com/newrelic/go-agent v3.9.0+incompatible
	github.com/shidenkai0/instrumentedsql v0.0.0-20180601154756-4a2a8bc1fde2
	github.com/shidenkai0/opentracing-contrib v0.0.0-20181012124239-69f936caf55e
	github.com/urfave/negroni v1.0.0
	gopkg.in/olivere/elastic.v5 v5.0.86
)
