package models

import (
	"compress/gzip"
	"io"
	"log"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"

	"fmt"

	"encoding/json"

	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
)

var (
	S3BucketName       = os.Getenv("AWS_S3_BUCKET")
	AwsAccessKeyId     = os.Getenv("AWS_ACCESS_KEY_ID")
	AwsSecretAccessKey = os.Getenv("AWS_SECRET_ACCESS_KEY")
	sess               = session.New(&aws.Config{
		Region:      aws.String(os.Getenv("AWS_REGION")),
		Credentials: credentials.NewStaticCredentials(AwsAccessKeyId, AwsSecretAccessKey, ""),
	})
	uploader = s3manager.NewUploader(sess)
	svc      = s3.New(sess)
)

// BackupParams stores parameters for a backup operation on the default database
type BackupParams struct {
	OlderThan time.Time
	Cleanup   bool
}

// sanitize backup parameters
func checkBackupParams(p BackupParams) BackupParams {
	return p // In preparation for more complicated parameters
}

// Backup starts a backup operation on the default database using the parameters specified by p
func Backup(p BackupParams) error {
	var err error
	p = checkBackupParams(p)
	if err != nil {
		log.Println("models: Crazy BackupParams", err)
		return err
	}
	sqlStr := "SELECT location[0] as lat, location[1] as lng, driver_id, state, created_at, speed, accuracy  FROM driver_traces WHERE created_at < $1;"
	rows, err := db.Queryx(sqlStr, p.OlderThan)
	if err != nil {
		log.Println("Trouble querying DB", err)
		return err
	}
	defer rows.Close()

	r, w := io.Pipe()
	defer r.Close()
	go func() {
		defer w.Close()
		gzipW := gzip.NewWriter(w)
		defer gzipW.Close()
		encoder := json.NewEncoder(gzipW)
		// Iterate through the results and add them to the resulting slice
		for rows.Next() {
			var t struct {
				ID        int       `json:"id" db:"driver_id"` // driver id
				Latitude  float64   `json:"lat" db:"lat"`
				Longitude float64   `json:"lng" db:"lng"`
				Status    int       `json:"status" db:"state"`
				Speed     float32   `json:"speed" db:"speed"`
				Accuracy  float32   `json:"accuracy" db:"accuracy"`
				CreatedAt time.Time `json:"created_at" db:"created_at"`
			}
			err = rows.StructScan(&t)
			if err != nil {
				log.Println("Trouble scanning results from DB query", err)
				continue
			}
			err = encoder.Encode(t)
			if err != nil {
				log.Println("Trouble encoding trace as JSON", err)
				continue
			}
		}
	}()
	var filename string
	if os.Getenv("RACK") == "" {
		filename = fmt.Sprintf("traces-%s.gz", time.Now().Format(time.RFC3339))
	} else {
		filename = fmt.Sprintf("%s/traces-%s.gz", os.Getenv("RACK"), time.Now().Format(time.RFC3339))
	}
	err = uploadToS3(r, filename)
	if err != nil {
		return err
	}
	if !p.Cleanup {
		return nil
	}
	log.Println("Deleting outdated driver traces after backup")
	_, err = db.Exec("DELETE FROM driver_traces WHERE created_at < $1;", p.OlderThan)
	return err
}

// UploadToS3 uploads the content of a given Reader to s3
func uploadToS3(body io.Reader, filename string) error {
	log.Println("Started s3 backup for driver_traces")
	_, err := uploader.Upload(&s3manager.UploadInput{
		Body:   body,
		Bucket: aws.String(S3BucketName),
		Key:    aws.String(filename),
	})

	if err != nil {
		log.Println("models: failed s3 upload", err)
	}

	fmt.Println("Finished s3 backup for driver_traces")

	return nil
}
