package models

import (
	"context"
	"log"
	"time"

	"bitbucket.org/yuso-itauto-dev/svc-geo"
	"bitbucket.org/yuso-itauto-dev/svc-history"
)

// TracesByTimeFrame gets driver traces for a given driver and time frame
func TracesByTimeFrame(ctx context.Context, driverId int, from time.Time, to time.Time) ([]geo.DriverTrace, error) {
	queryStr := "select location[0], location[1], state, sent_at, speed, accuracy from driver_traces where (driver_id = $1 and created_at between $2 and $3)"
	rows, err := db.QueryContext(ctx, queryStr, driverId, from, to)
	if err != nil {
		log.Println("Trouble querying DB", err)
		return nil, err
	}

	defer rows.Close()

	ts := make([]geo.DriverTrace, 0)

	// Iterate through the results and add them to the resulting slice
	for rows.Next() {
		var t geo.DriverTrace
		err = rows.Scan(&t.Location.Lng, &t.Location.Lat, &t.Status, &t.UpdatedAt, &t.Speed, &t.Accuracy)
		if err != nil {
			log.Println("Trouble scanning results from DB query", err)
			return nil, err
		}
		t.ID = driverId
		ts = append(ts, t)
	}

	return ts, nil
}

// AllTracesByTimeFrame gets driver traces for a given driver and time frame
func AllTracesByTimeFrame(ctx context.Context, from time.Time, to time.Time) ([]geo.DriverTrace, error) {
	queryStr := "select driver_id, location[0], location[1], state, sent_at, speed, accuracy from driver_traces where (created_at between $1 and $2)"
	rows, err := db.QueryContext(ctx, queryStr, from, to)
	if err != nil {
		log.Println("Trouble querying DB", err)
		return nil, err
	}

	defer rows.Close()

	ts := make([]geo.DriverTrace, 0)

	// Iterate through the results and add them to the resulting slice
	for rows.Next() {
		var t geo.DriverTrace
		err = rows.Scan(&t.ID, &t.Location.Lng, &t.Location.Lat, &t.Status, &t.UpdatedAt, &t.Speed, &t.Accuracy)
		if err != nil {
			log.Println("Trouble scanning results from DB query", err)
			return nil, err
		}
		ts = append(ts, t)
	}

	return ts, nil
}

func AddTrace(ctx context.Context, dt history.DriverTrace) error {
	return collector.Add(ctx, dt)
}
