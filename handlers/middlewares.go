package handlers

import (
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	newrelic "github.com/newrelic/go-agent"
	"github.com/urfave/negroni"
)

const (
	ContentType    = "Content-Type"
	Json           = "application/json"
	AcAllowOrigin  = "Access-Control-Allow-Origin"
	AcAllowMethods = "Access-Control-Allow-Methods"
	AcAllowHeaders = "Access-Control-Allow-Headers"
)

var (
	JsonContentType = headerContent{ContentType, Json}
	AllowOriginAll  = headerContent{AcAllowOrigin, "*"}
	AllowMethodsAll = headerContent{AcAllowMethods, "GET"}
	AllowStdHeaders = headerContent{AcAllowHeaders, "Content-Type, Authorization"}
)

type headerContent struct {
	name    string
	content string
}

func (hc headerContent) Parameters() (string, string) {
	return hc.name, hc.content
}

// corsHeaders sets the CORS headers of the response to allow access from other origins
func corsHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(AllowMethodsAll.Parameters())
		w.Header().Set(AllowOriginAll.Parameters())
		w.Header().Set(AllowStdHeaders.Parameters())
		next.ServeHTTP(w, r)
	})
}

func NewRelicMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if txn, ok := w.(newrelic.Transaction); ok {
			txn.SetName(r.URL.Path)
		}
		next.ServeHTTP(w, r)
	})

}

func JsonHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(JsonContentType.Parameters())
		next.ServeHTTP(w, r)
	})
}

// Logger is a middleware handler that logs the request as it goes in and the response as it goes out.
type Logger struct {
	// Logger inherits from log.Logger used to log messages with the Logger middleware
	*log.Logger
}

// NewLogger returns a new Logger instance
func NewLogger() *Logger {
	return &Logger{log.New(os.Stdout, "[History-Service] ", 0)}
}

// LoggingMiddleware is the actual middleware method provided by the logger, relies on negroni to provide request response
func (l *Logger) LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		l.Printf("Started %s %s", r.Method, r.URL.Path)
		next.ServeHTTP(w, r)
		res := w.(negroni.ResponseWriter)
		l.Printf("Completed %v %s in %v", res.Status(), http.StatusText(res.Status()), time.Since(start))
	})
}

func GetJwtSubCheckMiddleware(subs string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			splitSubs := strings.Split(subs, ",")
			token := r.Context().Value("user").(*jwt.Token)
			tokenSub := token.Claims.(jwt.MapClaims)["sub"]
			if tokenSub == nil || !stringInSlice(tokenSub.(string), splitSubs) {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
			next.ServeHTTP(w, r)
		})
	}
}

func stringInSlice(a string, slice []string) bool {
	for _, b := range slice {
		if b == a {
			return true
		}
	}
	return false
}
