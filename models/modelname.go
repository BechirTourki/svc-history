package models

import (
	"context"
)

type ModelName string

var modelEntries = [...]string{
	"action_to_request",
	"agenda_event",
	"customer",
	"customer_sms",
	"data_clip",
	"discount_code",
	"dispatch_process_configuration",
	"drivers_rule",
	"driver_zone",
	"favorite",
	"high_demand_zone",
	"package_type",
	"passenger_request",
	"phone_number",
	"pricing_rule",
	"pricing_zone",
	"ranking_zone",
	"recurrent_request",
	"saas_office",
	"vehicle_option",
	"vehicle_type",
	"zone",
}

func GetModelNames(ctx context.Context, scId int) ([]ModelName, error) {

	// const queryStr = "SELECT DISTINCT model_type from back_office_back_end_user_action_logs WHERE saas_company_id = $1;"

	// rows, err := db.QueryContext(ctx, queryStr, scId)
	// if err != nil {
	// 	log.Println("Trouble Querying DB", err)
	// 	return nil, err
	// }
	// defer rows.Close()

	// var mns []ModelName
	// for rows.Next() {
	// 	var mn string
	// 	if err = rows.Scan(&mn); err != nil {
	// 		log.Println("Trouble scanning results from DB query", err)
	// 		return nil, err
	// 	}
	// 	mns = append(mns, ModelName(mn))
	// }

	// SQL query is too long now
	// this dirty fix will do for now, later we need to create another table

	var mns []ModelName
	for _, modelName := range modelEntries {
		mns = append(mns, ModelName(modelName))
	}

	return mns, nil
}
