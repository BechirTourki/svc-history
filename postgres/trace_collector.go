package postgres

import (
	"context"

	"bitbucket.org/yuso-itauto-dev/svc-history"

	"github.com/jmoiron/sqlx"
)

type TraceCollector struct {
	db *sqlx.DB
}

func (tc *TraceCollector) Add(ctx context.Context, dt history.DriverTrace) error {
	queryStr := "insert into driver_traces(driver_id, location, state, sent_at, created_at, updated_at, speed, accuracy) values($1, Point($2, $3), $4, $5, now(), now(), $6, $7);"
	_, err := tc.db.ExecContext(ctx, queryStr, dt.ID, dt.Location.Lat, dt.Location.Lng, dt.Status, dt.SentAt, dt.Speed, dt.Accuracy)
	if err != nil {
		return err
	}
	return nil
}

func NewTraceCollector(db *sqlx.DB) *TraceCollector {
	return &TraceCollector{db: db}
}
