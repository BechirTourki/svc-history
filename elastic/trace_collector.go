package elastic

import (
	"context"
	"fmt"

	"bitbucket.org/yuso-itauto-dev/svc-history"

	"gopkg.in/olivere/elastic.v5"
)

type TraceCollector struct {
	client, tracesHistoryClient *elastic.Client
}

func (tc *TraceCollector) Add(ctx context.Context, dt history.DriverTrace) error {
	resp, err := tc.client.Index().Index(PerMinuteIndexName).Type(Type).Id(fmt.Sprintf("%d:%d:%d", dt.ID, dt.OfficeID, dt.SentAt.Unix()/60)).BodyJson(toElasticDriverTrace(dt)).Do(ctx)
	if err != nil {
		fmt.Println(resp, err)
		return err
	}
	// resp, err = tc.tracesHistoryClient.Index().Index(IndexName).Type(Type).BodyJson(toElasticDriverTrace(dt)).Do(ctx)
	// if err != nil {
	// 	fmt.Println(resp, err)
	// 	return err
	// }
	return nil
}

func NewTraceCollector(c, tracesHistoryClient *elastic.Client) *TraceCollector {
	return &TraceCollector{client: c, tracesHistoryClient: tracesHistoryClient}
}
